/*
Copyright 2016 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package etcd3

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"go.etcd.io/etcd/etcdserver/etcdserverpb"
	//runtimeOG "runtime"
	//"os"
	"path"
	"reflect"
	"strings"
	"time"

	"go.etcd.io/etcd/clientv3"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/conversion"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/apiserver/pkg/features"
	"k8s.io/apiserver/pkg/storage"
	"k8s.io/apiserver/pkg/storage/etcd3/metrics"
	"k8s.io/apiserver/pkg/storage/value"
	utilfeature "k8s.io/apiserver/pkg/util/feature"
	"k8s.io/klog/v2"
	utiltrace "k8s.io/utils/trace"
	//"runtime/debug"
)

// authenticatedDataString satisfies the value.Context interface. It uses the key to
// authenticate the stored data. This does not defend against reuse of previously
// encrypted values under the same key, but will prevent an attacker from using an
// encrypted value from a different key. A stronger authenticated data segment would
// include the etcd3 Version field (which is incremented on each write to a key and
// reset when the key is deleted), but an attacker with write access to etcd can
// force deletion and recreation of keys to weaken that angle.
type authenticatedDataString string

var ShortcutDeploymentNotifications chan *event
var ShortcutReplicasetNotifications chan *event
var ShortcutPodsNotifications chan *event

var UseSecondaryStorage bool
var UseShortcut bool

// AuthenticatedData implements the value.Context interface.
func (d authenticatedDataString) AuthenticatedData() []byte {
	return []byte(string(d))
}

var _ value.Context = authenticatedDataString("")

type store struct {
	client          *clientv3.Client
	clientSecondary *clientv3.Client
	codec           runtime.Codec
	versioner       storage.Versioner
	transformer     value.Transformer
	pathPrefix      string
	watcher         *watcher
	pagingEnabled   bool
	leaseManager    *leaseManager
}

type objState struct {
	obj   runtime.Object
	meta  *storage.ResponseMeta
	rev   int64
	data  []byte
	stale bool
}

// New returns an etcd3 implementation of storage.Interface.
func New(c *clientv3.Client, c2 *clientv3.Client, codec runtime.Codec, prefix string, transformer value.Transformer, pagingEnabled bool, leaseManagerConfig LeaseManagerConfig) storage.Interface {
	return newStore2(c, c2, pagingEnabled, codec, prefix, transformer, leaseManagerConfig)
}

func newStore2(c *clientv3.Client, c2 *clientv3.Client, pagingEnabled bool, codec runtime.Codec, prefix string, transformer value.Transformer, leaseManagerConfig LeaseManagerConfig) *store {
	versioner := APIObjectVersioner{}
	result := &store{
		client:          c,
		clientSecondary: c2,
		codec:           codec,
		versioner:       versioner,
		transformer:     transformer,
		pagingEnabled:   pagingEnabled,
		// for compatibility with etcd2 impl.
		// no-op for default prefix of '/registry'.
		// keeps compatibility with etcd2 impl for custom prefixes that don't start with '/'
		pathPrefix:   path.Join("/", prefix),
		watcher:      newWatcher(c, codec, versioner, transformer),
		leaseManager: newDefaultLeaseManager(c, leaseManagerConfig),
	}
	return result
}

func newStore(c *clientv3.Client, pagingEnabled bool, codec runtime.Codec, prefix string, transformer value.Transformer, leaseManagerConfig LeaseManagerConfig) *store {
	versioner := APIObjectVersioner{}
	result := &store{
		client:        c,
		codec:         codec,
		versioner:     versioner,
		transformer:   transformer,
		pagingEnabled: pagingEnabled,
		// for compatibility with etcd2 impl.
		// no-op for default prefix of '/registry'.
		// keeps compatibility with etcd2 impl for custom prefixes that don't start with '/'
		pathPrefix:   path.Join("/", prefix),
		watcher:      newWatcher(c, codec, versioner, transformer),
		leaseManager: newDefaultLeaseManager(c, leaseManagerConfig),
	}
	return result
}

// Versioner implements storage.Interface.Versioner.
func (s *store) Versioner() storage.Versioner {
	return s.versioner
}

// Get implements storage.Interface.Get.
func (s *store) Get(ctx context.Context, key string, opts storage.GetOptions, out runtime.Object) error {
	key = path.Join(s.pathPrefix, key)
	startTime := time.Now()
	getResp, err := s.client.KV.Get(ctx, key)
	metrics.RecordEtcdRequestLatency("get", getTypeName(out), startTime)
	if err != nil {
		return err
	}
	if err = s.validateMinimumResourceVersion(opts.ResourceVersion, uint64(getResp.Header.Revision)); err != nil {
		return err
	}

	if len(getResp.Kvs) == 0 {
		if opts.IgnoreNotFound {
			return runtime.SetZeroValue(out)
		}
		return storage.NewKeyNotFoundError(key, 0)
	}
	kv := getResp.Kvs[0]

	data, _, err := s.transformer.TransformFromStorage(kv.Value, authenticatedDataString(key))
	if err != nil {
		return storage.NewInternalError(err.Error())
	}

	return decode(s.codec, s.versioner, data, out, kv.ModRevision)
}

// Create implements storage.Interface.Create.
func (s *store) Create(ctx context.Context, key string, obj, out runtime.Object, ttl uint64) error {
	if version, err := s.versioner.ObjectResourceVersion(obj); err == nil && version != 0 {
		return errors.New("resourceVersion should not be set on objects to be created")
	}
	if err := s.versioner.PrepareObjectForStorage(obj); err != nil {
		return fmt.Errorf("PrepareObjectForStorage failed: %v", err)
	}
	data, err := runtime.Encode(s.codec, obj)
	if err != nil {
		return err
	}
	key = path.Join(s.pathPrefix, key)

	newData, err := s.transformer.TransformToStorage(data, authenticatedDataString(key))
	if err != nil {
		return storage.NewInternalError(err.Error())
	}

	var timeSinceHttpLatencyFunction time.Duration
	timeSinceHttpLatencyFunction = 0

	var canGoSecondaryStorage = false
	dictInter := ctx.Value("dict")
	if dictInter != nil {
		dict := dictInter.(map[string]interface{})
		tmpNow := time.Now()
		timeSinceHttpLatencyFunction = tmpNow.Sub(dict["http-latency-time"].(time.Time))
		dict["sinceHttpLatencyFunction"] = timeSinceHttpLatencyFunction
		dict["httpArrivalAtEtcdCall"] = tmpNow
		dict["etcdCallType"] = "create"
		canGoSecondaryStorage = dict["is-for-secondary-storage"].(bool)
	}
	var startTime time.Time
	var txnResp *clientv3.TxnResponse
	if canGoSecondaryStorage && UseSecondaryStorage {
		//opts, err := s.ttlOpts(ctx, int64(ttl), s.clientSecondary)
		//if opts != nil {
		//	klog.V(4).Infof("ttlOpts Secondary Create ttl %d   key %s   obj", ttl, key, obj.GetObjectKind().GroupVersionKind())
		//}
		//if err != nil {
		//	return err
		//}

		klog.V(4).Infof("Secondary: stored %s in alternative etcd storage (Create)", key)
		startTime = time.Now()
		// klog.InfoS("Secondary create: %s", key)
		txnResp, err = s.clientSecondary.KV.Txn(ctx).If(
			notFound(key),
		).Then(
			//clientv3.OpPut(key, string(newData), opts...),
			clientv3.OpPut(key, string(newData)),
		).Commit()
		if err != nil {
			klog.Infof("(Create) Secondary etcd storage error (%s): %v", key, err)
		}
	} else {
		// we still need to store this in secondary storage for potential later updates
		//var wg sync.WaitGroup
		//wg.Add(1)
		//go func() {
		//	defer wg.Done()
		//	txnResp, err = s.clientSecondary.KV.Txn(ctx).If(
		//		notFound(key),
		//	).Then(
		//		//clientv3.OpPut(key, string(newData), opts...),
		//		clientv3.OpPut(key, string(newData)),
		//	).Commit()
		//	if err != nil {
		//		klog.Infof("(Create) Secondary etcd storage error (within Primary path) (%s): %v", key, err)
		//	}
		//}()
		// normal storing in primary
		opts, err := s.ttlOpts(ctx, int64(ttl), s.client)
		if opts != nil {
			klog.V(4).Infof("ttlOpts Primary Create ttl %d   key %s   obj", ttl, key, obj.GetObjectKind().GroupVersionKind())
		}
		if err != nil {
			return err
		}

		klog.V(4).Infof("Primary: stored %s in main etcd storage (Create)", key)
		//klog.InfoS("Primary create: %s", key)
		startTime = time.Now()
		txnResp, err = s.client.KV.Txn(ctx).If(
			notFound(key),
		).Then(
			clientv3.OpPut(key, string(newData), opts...),
		).Commit()
		metrics.RecordEtcdRequestLatency("create", getTypeName(obj), startTime)
		// wg.Wait() // waiting for the secondary storage (shouldn't really have to wait here)
		if err != nil {
			klog.V(4).Infof("Primary Commit() failed with: %v", err)
			return err
		}
		if txnResp == nil {
			klog.V(4).Infof("txnResp is nil: %v", err)
		}

	}

	afterTime := time.Now()
	//fmt.Fprintf(os.Stderr, "etcd-create-latency:[%v][uuid:%v][sinceHttp:%v][compare:%t]\n",
	//	, ctx.Value("uuid-tracking"), timeSinceHttpLatencyFunction, txnResp.Succeeded)
	etcdLatency := afterTime.Sub(startTime)
	if dictInter != nil {
		dict := dictInter.(map[string]interface{})
		dict["sincePostEtcd"] = afterTime
		dict["httpArrivalAtEtcdDone"] = afterTime
		if txnResp != nil {
			dict["conditionalIfTnxSucceeded"] = txnResp.Succeeded
		}
		dict["etcdLatency"] = etcdLatency
	}

	//	debug.PrintStack()
	var putResp *etcdserverpb.PutResponse

	if !txnResp.Succeeded {
		return storage.NewKeyExistsError(key, 0)
	} else {
		putResp = txnResp.Responses[0].GetResponsePut()
	}
	//temp
	//if strings.HasPrefix(key, "/registry/deployments/default") {
	//	filename := fmt.Sprintf("/tmp/http__%s", strings.ReplaceAll(key, "/", "_"))
	//	ioutil.WriteFile(filename, []byte(hex.EncodeToString(data)), 0777)
	//
	//	filename = fmt.Sprintf("/tmp/httpmeta__%s", strings.ReplaceAll(key, "/", "_"))
	//	content := fmt.Sprintf("Rev:%d", putResp.Header.Revision)
	//	ioutil.WriteFile(filename, []byte(content), 0777)
	//}

	if UseShortcut && ShortcutDeploymentNotifications != nil && (strings.HasPrefix(key, "/registry/deployments/default")) {
		notification := &event{
			key:       key,
			value:     data,
			prevValue: nil,
			rev:       putResp.Header.Revision,
			isDeleted: false,
			isCreated: true,
		}
		klog.V(4).Infof("deploy createkey1:%s (%d) [rev:%d] [val1:%s]", key, len(ShortcutDeploymentNotifications), putResp.Header.Revision, hex.EncodeToString(data))
		//klog.Infof("deploy createkey2:%s [rev:%d] [val2:%s]", key, putResp.Header.Revision, hex.EncodeToString(newData))
		select {
		case ShortcutDeploymentNotifications <- notification:
		default:
			klog.Infof("ShortcutDeploymentNotifications full. Discarding notification for key %s", key)
		}
		// val1 good

	}

	if UseShortcut && ShortcutReplicasetNotifications != nil && (strings.HasPrefix(key, "/registry/replicasets/default")) {
		notification := &event{
			key:       key,
			value:     data,
			prevValue: nil,
			rev:       putResp.Header.Revision,
			isDeleted: false,
			isCreated: true,
		}
		klog.Infof("rs createkey1:%s (%d) [rev:%d] [val1:%s]", key, len(ShortcutReplicasetNotifications), putResp.Header.Revision, hex.EncodeToString(data))
		//klog.Infof("createkey2:%s [rev:%d] [val2:%s]", key, putResp.Header.Revision, hex.EncodeToString(newData))
		select {
		case ShortcutReplicasetNotifications <- notification:
		default:
			klog.Infof("ShortcutReplicasetNotifications full. Discarding notification for key %s", key)
		}
		// val1 good

	}

	if UseShortcut && ShortcutPodsNotifications != nil && (strings.HasPrefix(key, "/registry/pods/default")) {
		notification := &event{
			key:       key,
			value:     data,
			prevValue: nil,
			rev:       putResp.Header.Revision,
			isDeleted: false,
			isCreated: true,
		}
		klog.Infof("pods createkey1:%s (%d) [rev:%d] [val1:%s]", key, len(ShortcutPodsNotifications), putResp.Header.Revision, hex.EncodeToString(data))
		//klog.Infof("createkey2:%s [rev:%d] [val2:%s]", key, putResp.Header.Revision, hex.EncodeToString(newData))
		select {
		case ShortcutPodsNotifications <- notification:
		default:
			klog.Infof("ShortcutPodsNotifications full. Discarding notification for key %s", key)
		}
		// val1 good

	}

	if out != nil {
		return decode(s.codec, s.versioner, data, out, putResp.Header.Revision)
	}
	return nil
}

// Delete implements storage.Interface.Delete.
func (s *store) Delete(ctx context.Context, key string, out runtime.Object, preconditions *storage.Preconditions, validateDeletion storage.ValidateObjectFunc) error {
	v, err := conversion.EnforcePtr(out)
	if err != nil {
		return fmt.Errorf("unable to convert output object to pointer: %v", err)
	}
	key = path.Join(s.pathPrefix, key)
	return s.conditionalDelete(ctx, key, out, v, preconditions, validateDeletion)
}

func (s *store) conditionalDelete(ctx context.Context, key string, out runtime.Object, v reflect.Value, preconditions *storage.Preconditions, validateDeletion storage.ValidateObjectFunc) error {
	startTime := time.Now()
	getResp, err := s.client.KV.Get(ctx, key)
	metrics.RecordEtcdRequestLatency("get", getTypeName(out), startTime)
	if err != nil {
		return err
	}
	for {
		origState, err := s.getState(getResp, key, v, false)
		if err != nil {
			return err
		}
		if preconditions != nil {
			if err := preconditions.Check(key, origState.obj); err != nil {
				return err
			}
		}
		if err := validateDeletion(ctx, origState.obj); err != nil {
			return err
		}
		dictInter := ctx.Value("dict")

		var timeSinceHttpLatencyFunction time.Duration

		if dictInter != nil {
			dict := dictInter.(map[string]interface{})
			tmpNow := time.Now()
			timeSinceHttpLatencyFunction = tmpNow.Sub(dict["http-latency-time"].(time.Time))
			dict["sinceHttpLatencyFunction"] = timeSinceHttpLatencyFunction
			dict["etcdCallType"] = "delete"
			dict["httpArrivalAtEtcdCall"] = tmpNow
		}

		startTime := time.Now()
		txnResp, err := s.client.KV.Txn(ctx).If(
			clientv3.Compare(clientv3.ModRevision(key), "=", origState.rev),
		).Then(
			clientv3.OpDelete(key),
		).Else(
			clientv3.OpGet(key),
		).Commit()
		metrics.RecordEtcdRequestLatency("delete", getTypeName(out), startTime)
		afterTime := time.Now()
		etcdLatency := afterTime.Sub(startTime)
		if dictInter != nil {
			dict := dictInter.(map[string]interface{})
			dict["sincePostEtcd"] = afterTime
			dict["httpArrivalAtEtcdDone"] = afterTime
			if txnResp != nil {
				dict["conditionalIfTnxSucceeded"] = txnResp.Succeeded
			}
			dict["etcdLatency"] = etcdLatency
		}
		if err != nil {
			return err
		}
		if !txnResp.Succeeded {
			getResp = (*clientv3.GetResponse)(txnResp.Responses[0].GetResponseRange())
			klog.V(4).Infof("deletion of %s failed because of a conflict, going to retry", key)
			continue
		}
		return decode(s.codec, s.versioner, origState.data, out, origState.rev)
	}
}

// GuaranteedUpdate implements storage.Interface.GuaranteedUpdate.
func (s *store) GuaranteedUpdate(
	ctx context.Context, key string, out runtime.Object, ignoreNotFound bool,
	preconditions *storage.Preconditions, tryUpdate storage.UpdateFunc, suggestion ...runtime.Object) error {
	trace := utiltrace.New("GuaranteedUpdate etcd3", utiltrace.Field{"type", getTypeName(out)})
	defer trace.LogIfLong(5000 * time.Millisecond)

	v, err := conversion.EnforcePtr(out)
	if err != nil {
		return fmt.Errorf("unable to convert output object to pointer: %v", err)
	}
	key = path.Join(s.pathPrefix, key)

	getCurrentState := func() (*objState, error) {
		startTime := time.Now()
		getResp, err := s.client.KV.Get(ctx, key)
		metrics.RecordEtcdRequestLatency("get", getTypeName(out), startTime)
		if err != nil {
			return nil, err
		}
		return s.getState(getResp, key, v, ignoreNotFound)
	}

	var origState *objState
	var mustCheckData bool
	if len(suggestion) == 1 && suggestion[0] != nil {
		origState, err = s.getStateFromObject(suggestion[0])
		if err != nil {
			return err
		}
		mustCheckData = true
	} else {
		origState, err = getCurrentState()
		if err != nil {
			return err
		}
	}
	trace.Step("initial value restored")

	transformContext := authenticatedDataString(key)
	for {
		if err := preconditions.Check(key, origState.obj); err != nil {
			// If our data is already up to date, return the error
			if !mustCheckData {
				return err
			}

			// It's possible we were working with stale data
			// Actually fetch
			origState, err = getCurrentState()
			if err != nil {
				return err
			}
			mustCheckData = false
			// Retry
			continue
		}

		ret, ttl, err := s.updateState(origState, tryUpdate)
		if err != nil {
			// If our data is already up to date, return the error
			if !mustCheckData {
				return err
			}

			// It's possible we were working with stale data
			// Actually fetch
			origState, err = getCurrentState()
			if err != nil {
				return err
			}
			mustCheckData = false
			// Retry
			continue
		}

		data, err := runtime.Encode(s.codec, ret)
		if err != nil {
			return err
		}
		if !origState.stale && bytes.Equal(data, origState.data) {
			// if we skipped the original Get in this loop, we must refresh from
			// etcd in order to be sure the data in the store is equivalent to
			// our desired serialization
			if mustCheckData {
				origState, err = getCurrentState()
				if err != nil {
					return err
				}
				mustCheckData = false
				if !bytes.Equal(data, origState.data) {
					// original data changed, restart loop
					continue
				}
			}
			// recheck that the data from etcd is not stale before short-circuiting a write
			if !origState.stale {
				return decode(s.codec, s.versioner, origState.data, out, origState.rev)
			}
		}

		newData, err := s.transformer.TransformToStorage(data, transformContext)
		if err != nil {
			return storage.NewInternalError(err.Error())
		}
		trace.Step("Transaction prepared")
		var canGoSecondaryStorage = false
		var timeSinceHttpLatencyFunction time.Duration
		timeSinceHttpLatencyFunction = 0

		dictInter := ctx.Value("dict")
		if dictInter != nil {
			dict := dictInter.(map[string]interface{})
			tmpNow := time.Now()
			timeSinceHttpLatencyFunction = tmpNow.Sub(dict["http-latency-time"].(time.Time))
			dict["sinceHttpLatencyFunction"] = timeSinceHttpLatencyFunction
			dict["httpArrivalAtEtcdCall"] = tmpNow
			dict["etcdCallType"] = "update"
			canGoSecondaryStorage = dict["is-for-secondary-storage"].(bool)
		}

		var startTime time.Time
		var txnResp *clientv3.TxnResponse
		var putRespSecondaryStorage *clientv3.PutResponse

		if canGoSecondaryStorage && UseSecondaryStorage {

			opts, err := s.ttlOpts(ctx, int64(ttl), s.clientSecondary)
			if opts != nil {
				klog.V(4).Infof("ttlOpts Secondary Update ttl %d   key %s   obj", ttl, key, out.GetObjectKind().GroupVersionKind())
			}
			if err != nil {
				return err
			}

			// ORIGINAL UPDATE
			klog.V(4).Infof("Secondary: stored %s in alternative etcd storage (Update)", key)
			//startTime = time.Now()
			//txnResp, err = s.clientSecondary.KV.Txn(ctx).If(
			//	clientv3.Compare(clientv3.ModRevision(key), "=", origState.rev),
			//).Then(
			//	clientv3.OpPut(key, string(newData), opts...),
			//).Else(
			//	clientv3.OpGet(key),
			//).Commit()
			//if err != nil {
			//	klog.Infof("(Update) Secondary etcd storage error (%s): %v", key, err)
			//}
			// klog.InfoS("Secondary update: %s", key)
			putRespSecondaryStorage, err = s.clientSecondary.KV.Put(context.Background(), key, string(newData))
			if err != nil {
				klog.Infof("(Update Simple) Secondary etcd storage error (%s): %v", key, err)
			}

		} else {

			opts, err := s.ttlOpts(ctx, int64(ttl), s.client)
			if opts != nil {
				klog.V(4).Infof("ttlOpts Primary Update ttl %d   key %s   obj", ttl, key, out.GetObjectKind().GroupVersionKind())
			}
			if err != nil {
				return err
			}

			klog.V(4).Infof("Primary: stored %s in main etcd storage (Update)", key)
			startTime = time.Now()
			//klog.InfoS("Primary update: %s", key)
			txnResp, err = s.client.KV.Txn(ctx).If(
				clientv3.Compare(clientv3.ModRevision(key), "=", origState.rev),
			).Then(
				clientv3.OpPut(key, string(newData), opts...),
			).Else(
				clientv3.OpGet(key),
			).Commit()
			if err != nil {
				klog.Infof("(Update) Primary etcd storage error (%s): %v", key, err)
				return err
			}
		}

		afterTime := time.Now()
		//fmt.Fprintf(os.Stderr, "etcd-update-latency:[%v][uuid:%v][sinceHttp:%v][compare:%v]\n",
		//	afterTime.Sub(startTime), ctx.Value("uuid-tracking"), timeSinceHttpLatencyFunction, txnResp.Succeeded)
		etcdLatency := afterTime.Sub(startTime)
		if dictInter != nil {
			dict := dictInter.(map[string]interface{})
			dict["sincePostEtcd"] = afterTime
			dict["httpArrivalAtEtcdDone"] = afterTime
			if txnResp != nil {
				dict["conditionalIfTnxSucceeded"] = txnResp.Succeeded
			}
			dict["etcdLatency"] = etcdLatency
		}

		//debug.PrintStack()
		metrics.RecordEtcdRequestLatency("update", getTypeName(out), startTime)
		//if err != nil {
		//	return err
		//}
		trace.Step("Transaction committed")
		if txnResp != nil && !txnResp.Succeeded {
			getResp := (*clientv3.GetResponse)(txnResp.Responses[0].GetResponseRange())
			klog.V(4).Infof("GuaranteedUpdate of %s failed because of a conflict, going to retry", key)
			origState, err = s.getState(getResp, key, v, ignoreNotFound)
			if err != nil {
				return err
			}
			trace.Step("Retry value restored")
			mustCheckData = false
			continue
		}

		if txnResp != nil {
			putResp := txnResp.Responses[0].GetResponsePut()
			return decode(s.codec, s.versioner, data, out, putResp.Header.Revision)
		} else if putRespSecondaryStorage != nil {
			putResp := putRespSecondaryStorage
			return decode(s.codec, s.versioner, data, out, putResp.Header.Revision)
		} else {
			panic("neither txnResp nor putRespSecondaryStorage are valid")
		}

	}
}

// GetToList implements storage.Interface.GetToList.
func (s *store) GetToList(ctx context.Context, key string, listOpts storage.ListOptions, listObj runtime.Object) error {
	resourceVersion := listOpts.ResourceVersion
	match := listOpts.ResourceVersionMatch
	pred := listOpts.Predicate
	trace := utiltrace.New("GetToList etcd3",
		utiltrace.Field{"key", key},
		utiltrace.Field{"resourceVersion", resourceVersion},
		utiltrace.Field{"resourceVersionMatch", match},
		utiltrace.Field{"limit", pred.Limit},
		utiltrace.Field{"continue", pred.Continue})
	defer trace.LogIfLong(5000 * time.Millisecond)
	listPtr, err := meta.GetItemsPtr(listObj)
	if err != nil {
		return err
	}
	v, err := conversion.EnforcePtr(listPtr)
	if err != nil || v.Kind() != reflect.Slice {
		return fmt.Errorf("need ptr to slice: %v", err)
	}

	newItemFunc := getNewItemFunc(listObj, v)

	key = path.Join(s.pathPrefix, key)
	startTime := time.Now()
	var opts []clientv3.OpOption
	if len(resourceVersion) > 0 && match == metav1.ResourceVersionMatchExact {
		rv, err := s.versioner.ParseResourceVersion(resourceVersion)
		if err != nil {
			return apierrors.NewBadRequest(fmt.Sprintf("invalid resource version: %v", err))
		}
		opts = append(opts, clientv3.WithRev(int64(rv)))
	}

	getResp, err := s.client.KV.Get(ctx, key, opts...)
	metrics.RecordEtcdRequestLatency("get", getTypeName(listPtr), startTime)
	if err != nil {
		return err
	}
	if err = s.validateMinimumResourceVersion(resourceVersion, uint64(getResp.Header.Revision)); err != nil {
		return err
	}

	if len(getResp.Kvs) > 0 {
		data, _, err := s.transformer.TransformFromStorage(getResp.Kvs[0].Value, authenticatedDataString(key))
		if err != nil {
			return storage.NewInternalError(err.Error())
		}
		if err := appendListItem(v, data, uint64(getResp.Kvs[0].ModRevision), pred, s.codec, s.versioner, newItemFunc); err != nil {
			return err
		}
	}
	// update version with cluster level revision
	return s.versioner.UpdateList(listObj, uint64(getResp.Header.Revision), "", nil)
}

func getNewItemFunc(listObj runtime.Object, v reflect.Value) func() runtime.Object {
	// For unstructured lists with a target group/version, preserve the group/version in the instantiated list items
	if unstructuredList, isUnstructured := listObj.(*unstructured.UnstructuredList); isUnstructured {
		if apiVersion := unstructuredList.GetAPIVersion(); len(apiVersion) > 0 {
			return func() runtime.Object {
				return &unstructured.Unstructured{Object: map[string]interface{}{"apiVersion": apiVersion}}
			}
		}
	}

	// Otherwise just instantiate an empty item
	elem := v.Type().Elem()
	return func() runtime.Object {
		return reflect.New(elem).Interface().(runtime.Object)
	}
}

func (s *store) Count(key string) (int64, error) {
	key = path.Join(s.pathPrefix, key)

	// We need to make sure the key ended with "/" so that we only get children "directories".
	// e.g. if we have key "/a", "/a/b", "/ab", getting keys with prefix "/a" will return all three,
	// while with prefix "/a/" will return only "/a/b" which is the correct answer.
	if !strings.HasSuffix(key, "/") {
		key += "/"
	}

	startTime := time.Now()
	getResp, err := s.client.KV.Get(context.Background(), key, clientv3.WithRange(clientv3.GetPrefixRangeEnd(key)), clientv3.WithCountOnly())
	metrics.RecordEtcdRequestLatency("listWithCount", key, startTime)
	if err != nil {
		return 0, err
	}
	return getResp.Count, nil
}

// continueToken is a simple structured object for encoding the state of a continue token.
// TODO: if we change the version of the encoded from, we can't start encoding the new version
// until all other servers are upgraded (i.e. we need to support rolling schema)
// This is a public API struct and cannot change.
type continueToken struct {
	APIVersion      string `json:"v"`
	ResourceVersion int64  `json:"rv"`
	StartKey        string `json:"start"`
}

// parseFrom transforms an encoded predicate from into a versioned struct.
// TODO: return a typed error that instructs clients that they must relist
func decodeContinue(continueValue, keyPrefix string) (fromKey string, rv int64, err error) {
	data, err := base64.RawURLEncoding.DecodeString(continueValue)
	if err != nil {
		return "", 0, fmt.Errorf("continue key is not valid: %v", err)
	}
	var c continueToken
	if err := json.Unmarshal(data, &c); err != nil {
		return "", 0, fmt.Errorf("continue key is not valid: %v", err)
	}
	switch c.APIVersion {
	case "meta.k8s.io/v1":
		if c.ResourceVersion == 0 {
			return "", 0, fmt.Errorf("continue key is not valid: incorrect encoded start resourceVersion (version meta.k8s.io/v1)")
		}
		if len(c.StartKey) == 0 {
			return "", 0, fmt.Errorf("continue key is not valid: encoded start key empty (version meta.k8s.io/v1)")
		}
		// defend against path traversal attacks by clients - path.Clean will ensure that startKey cannot
		// be at a higher level of the hierarchy, and so when we append the key prefix we will end up with
		// continue start key that is fully qualified and cannot range over anything less specific than
		// keyPrefix.
		key := c.StartKey
		if !strings.HasPrefix(key, "/") {
			key = "/" + key
		}
		cleaned := path.Clean(key)
		if cleaned != key {
			return "", 0, fmt.Errorf("continue key is not valid: %s", c.StartKey)
		}
		return keyPrefix + cleaned[1:], c.ResourceVersion, nil
	default:
		return "", 0, fmt.Errorf("continue key is not valid: server does not recognize this encoded version %q", c.APIVersion)
	}
}

// encodeContinue returns a string representing the encoded continuation of the current query.
func encodeContinue(key, keyPrefix string, resourceVersion int64) (string, error) {
	nextKey := strings.TrimPrefix(key, keyPrefix)
	if nextKey == key {
		return "", fmt.Errorf("unable to encode next field: the key and key prefix do not match")
	}
	out, err := json.Marshal(&continueToken{APIVersion: "meta.k8s.io/v1", ResourceVersion: resourceVersion, StartKey: nextKey})
	if err != nil {
		return "", err
	}
	return base64.RawURLEncoding.EncodeToString(out), nil
}

// List implements storage.Interface.List.
func (s *store) List(ctx context.Context, key string, opts storage.ListOptions, listObj runtime.Object) error {
	resourceVersion := opts.ResourceVersion
	match := opts.ResourceVersionMatch
	pred := opts.Predicate
	trace := utiltrace.New("List etcd3",
		utiltrace.Field{"key", key},
		utiltrace.Field{"resourceVersion", resourceVersion},
		utiltrace.Field{"resourceVersionMatch", match},
		utiltrace.Field{"limit", pred.Limit},
		utiltrace.Field{"continue", pred.Continue})
	defer trace.LogIfLong(5000 * time.Millisecond)
	listPtr, err := meta.GetItemsPtr(listObj)
	if err != nil {
		return err
	}
	v, err := conversion.EnforcePtr(listPtr)
	if err != nil || v.Kind() != reflect.Slice {
		return fmt.Errorf("need ptr to slice: %v", err)
	}

	if s.pathPrefix != "" {
		key = path.Join(s.pathPrefix, key)
	}
	// We need to make sure the key ended with "/" so that we only get children "directories".
	// e.g. if we have key "/a", "/a/b", "/ab", getting keys with prefix "/a" will return all three,
	// while with prefix "/a/" will return only "/a/b" which is the correct answer.
	if !strings.HasSuffix(key, "/") {
		key += "/"
	}
	keyPrefix := key

	// set the appropriate clientv3 options to filter the returned data set
	var paging bool
	options := make([]clientv3.OpOption, 0, 4)
	if s.pagingEnabled && pred.Limit > 0 {
		paging = true
		options = append(options, clientv3.WithLimit(pred.Limit))
	}

	newItemFunc := getNewItemFunc(listObj, v)

	var fromRV *uint64
	if len(resourceVersion) > 0 {
		parsedRV, err := s.versioner.ParseResourceVersion(resourceVersion)
		if err != nil {
			return apierrors.NewBadRequest(fmt.Sprintf("invalid resource version: %v", err))
		}
		fromRV = &parsedRV
	}

	var returnedRV, continueRV, withRev int64
	var continueKey string
	switch {
	case s.pagingEnabled && len(pred.Continue) > 0:
		continueKey, continueRV, err = decodeContinue(pred.Continue, keyPrefix)
		if err != nil {
			return apierrors.NewBadRequest(fmt.Sprintf("invalid continue token: %v", err))
		}

		if len(resourceVersion) > 0 && resourceVersion != "0" {
			return apierrors.NewBadRequest("specifying resource version is not allowed when using continue")
		}

		rangeEnd := clientv3.GetPrefixRangeEnd(keyPrefix)
		options = append(options, clientv3.WithRange(rangeEnd))
		key = continueKey

		// If continueRV > 0, the LIST request needs a specific resource version.
		// continueRV==0 is invalid.
		// If continueRV < 0, the request is for the latest resource version.
		if continueRV > 0 {
			withRev = continueRV
			returnedRV = continueRV
		}
	case s.pagingEnabled && pred.Limit > 0:
		if fromRV != nil {
			switch match {
			case metav1.ResourceVersionMatchNotOlderThan:
				// The not older than constraint is checked after we get a response from etcd,
				// and returnedRV is then set to the revision we get from the etcd response.
			case metav1.ResourceVersionMatchExact:
				returnedRV = int64(*fromRV)
				withRev = returnedRV
			case "": // legacy case
				if *fromRV > 0 {
					returnedRV = int64(*fromRV)
					withRev = returnedRV
				}
			default:
				return fmt.Errorf("unknown ResourceVersionMatch value: %v", match)
			}
		}

		rangeEnd := clientv3.GetPrefixRangeEnd(keyPrefix)
		options = append(options, clientv3.WithRange(rangeEnd))
	default:
		if fromRV != nil {
			switch match {
			case metav1.ResourceVersionMatchNotOlderThan:
				// The not older than constraint is checked after we get a response from etcd,
				// and returnedRV is then set to the revision we get from the etcd response.
			case metav1.ResourceVersionMatchExact:
				returnedRV = int64(*fromRV)
				withRev = returnedRV
			case "": // legacy case
			default:
				return fmt.Errorf("unknown ResourceVersionMatch value: %v", match)
			}
		}

		options = append(options, clientv3.WithPrefix())
	}
	if withRev != 0 {
		options = append(options, clientv3.WithRev(withRev))
	}

	// loop until we have filled the requested limit from etcd or there are no more results
	var lastKey []byte
	var hasMore bool
	var getResp *clientv3.GetResponse
	for {
		startTime := time.Now()
		getResp, err = s.client.KV.Get(ctx, key, options...)
		metrics.RecordEtcdRequestLatency("list", getTypeName(listPtr), startTime)
		if err != nil {
			return interpretListError(err, len(pred.Continue) > 0, continueKey, keyPrefix)
		}
		if err = s.validateMinimumResourceVersion(resourceVersion, uint64(getResp.Header.Revision)); err != nil {
			return err
		}
		hasMore = getResp.More

		if len(getResp.Kvs) == 0 && getResp.More {
			return fmt.Errorf("no results were found, but etcd indicated there were more values remaining")
		}

		// avoid small allocations for the result slice, since this can be called in many
		// different contexts and we don't know how significantly the result will be filtered
		if pred.Empty() {
			growSlice(v, len(getResp.Kvs))
		} else {
			growSlice(v, 2048, len(getResp.Kvs))
		}

		// take items from the response until the bucket is full, filtering as we go
		for _, kv := range getResp.Kvs {
			if paging && int64(v.Len()) >= pred.Limit {
				hasMore = true
				break
			}
			lastKey = kv.Key

			data, _, err := s.transformer.TransformFromStorage(kv.Value, authenticatedDataString(kv.Key))
			if err != nil {
				return storage.NewInternalErrorf("unable to transform key %q: %v", kv.Key, err)
			}

			if err := appendListItem(v, data, uint64(kv.ModRevision), pred, s.codec, s.versioner, newItemFunc); err != nil {
				return err
			}
		}

		// indicate to the client which resource version was returned
		if returnedRV == 0 {
			returnedRV = getResp.Header.Revision
		}

		// no more results remain or we didn't request paging
		if !hasMore || !paging {
			break
		}
		// we're paging but we have filled our bucket
		if int64(v.Len()) >= pred.Limit {
			break
		}
		key = string(lastKey) + "\x00"
		if withRev == 0 {
			withRev = returnedRV
			options = append(options, clientv3.WithRev(withRev))
		}
	}

	// instruct the client to begin querying from immediately after the last key we returned
	// we never return a key that the client wouldn't be allowed to see
	if hasMore {
		// we want to start immediately after the last key
		next, err := encodeContinue(string(lastKey)+"\x00", keyPrefix, returnedRV)
		if err != nil {
			return err
		}
		var remainingItemCount *int64
		// getResp.Count counts in objects that do not match the pred.
		// Instead of returning inaccurate count for non-empty selectors, we return nil.
		// Only set remainingItemCount if the predicate is empty.
		if utilfeature.DefaultFeatureGate.Enabled(features.RemainingItemCount) {
			if pred.Empty() {
				c := int64(getResp.Count - pred.Limit)
				remainingItemCount = &c
			}
		}
		return s.versioner.UpdateList(listObj, uint64(returnedRV), next, remainingItemCount)
	}

	// no continuation
	return s.versioner.UpdateList(listObj, uint64(returnedRV), "", nil)
}

// growSlice takes a slice value and grows its capacity up
// to the maximum of the passed sizes or maxCapacity, whichever
// is smaller. Above maxCapacity decisions about allocation are left
// to the Go runtime on append. This allows a caller to make an
// educated guess about the potential size of the total list while
// still avoiding overly aggressive initial allocation. If sizes
// is empty maxCapacity will be used as the size to grow.
func growSlice(v reflect.Value, maxCapacity int, sizes ...int) {
	cap := v.Cap()
	max := cap
	for _, size := range sizes {
		if size > max {
			max = size
		}
	}
	if len(sizes) == 0 || max > maxCapacity {
		max = maxCapacity
	}
	if max <= cap {
		return
	}
	if v.Len() > 0 {
		extra := reflect.MakeSlice(v.Type(), 0, max)
		reflect.Copy(extra, v)
		v.Set(extra)
	} else {
		extra := reflect.MakeSlice(v.Type(), 0, max)
		v.Set(extra)
	}
}

// Watch implements storage.Interface.Watch.
func (s *store) Watch(ctx context.Context, key string, opts storage.ListOptions) (watch.Interface, error) {
	return s.watch(ctx, key, opts, false)
}

// WatchList implements storage.Interface.WatchList.
func (s *store) WatchList(ctx context.Context, key string, opts storage.ListOptions) (watch.Interface, error) {
	return s.watch(ctx, key, opts, true)
}

func (s *store) watch(ctx context.Context, key string, opts storage.ListOptions, recursive bool) (watch.Interface, error) {
	rev, err := s.versioner.ParseResourceVersion(opts.ResourceVersion)
	if err != nil {
		return nil, err
	}
	key = path.Join(s.pathPrefix, key)
	return s.watcher.Watch(ctx, key, int64(rev), recursive, opts.Predicate)
}

func (s *store) getState(getResp *clientv3.GetResponse, key string, v reflect.Value, ignoreNotFound bool) (*objState, error) {
	state := &objState{
		meta: &storage.ResponseMeta{},
	}

	if u, ok := v.Addr().Interface().(runtime.Unstructured); ok {
		state.obj = u.NewEmptyInstance()
	} else {
		state.obj = reflect.New(v.Type()).Interface().(runtime.Object)
	}

	if len(getResp.Kvs) == 0 {
		if !ignoreNotFound {
			return nil, storage.NewKeyNotFoundError(key, 0)
		}
		if err := runtime.SetZeroValue(state.obj); err != nil {
			return nil, err
		}
	} else {
		data, stale, err := s.transformer.TransformFromStorage(getResp.Kvs[0].Value, authenticatedDataString(key))
		if err != nil {
			return nil, storage.NewInternalError(err.Error())
		}
		state.rev = getResp.Kvs[0].ModRevision
		state.meta.ResourceVersion = uint64(state.rev)
		state.data = data
		state.stale = stale
		if err := decode(s.codec, s.versioner, state.data, state.obj, state.rev); err != nil {
			return nil, err
		}
	}
	return state, nil
}

func (s *store) getStateFromObject(obj runtime.Object) (*objState, error) {
	state := &objState{
		obj:  obj,
		meta: &storage.ResponseMeta{},
	}

	rv, err := s.versioner.ObjectResourceVersion(obj)
	if err != nil {
		return nil, fmt.Errorf("couldn't get resource version: %v", err)
	}
	state.rev = int64(rv)
	state.meta.ResourceVersion = uint64(state.rev)

	// Compute the serialized form - for that we need to temporarily clean
	// its resource version field (those are not stored in etcd).
	if err := s.versioner.PrepareObjectForStorage(obj); err != nil {
		return nil, fmt.Errorf("PrepareObjectForStorage failed: %v", err)
	}
	state.data, err = runtime.Encode(s.codec, obj)
	if err != nil {
		return nil, err
	}
	if err := s.versioner.UpdateObject(state.obj, uint64(rv)); err != nil {
		klog.Errorf("failed to update object version: %v", err)
	}
	return state, nil
}

func (s *store) updateState(st *objState, userUpdate storage.UpdateFunc) (runtime.Object, uint64, error) {
	ret, ttlPtr, err := userUpdate(st.obj, *st.meta)
	if err != nil {
		return nil, 0, err
	}

	if err := s.versioner.PrepareObjectForStorage(ret); err != nil {
		return nil, 0, fmt.Errorf("PrepareObjectForStorage failed: %v", err)
	}
	var ttl uint64
	if ttlPtr != nil {
		ttl = *ttlPtr
	}
	return ret, ttl, nil
}

// ttlOpts returns client options based on given ttl.
// ttl: if ttl is non-zero, it will attach the key to a lease with ttl of roughly the same length
func (s *store) ttlOpts(ctx context.Context, ttl int64, client *clientv3.Client) ([]clientv3.OpOption, error) {
	if ttl == 0 {
		return nil, nil
	}
	id, err := s.leaseManager.GetLease(ctx, ttl, client)
	if err != nil {
		return nil, err
	}
	return []clientv3.OpOption{clientv3.WithLease(id)}, nil
}

// validateMinimumResourceVersion returns a 'too large resource' version error when the provided minimumResourceVersion is
// greater than the most recent actualRevision available from storage.
func (s *store) validateMinimumResourceVersion(minimumResourceVersion string, actualRevision uint64) error {
	if minimumResourceVersion == "" {
		return nil
	}
	minimumRV, err := s.versioner.ParseResourceVersion(minimumResourceVersion)
	if err != nil {
		return apierrors.NewBadRequest(fmt.Sprintf("invalid resource version: %v", err))
	}
	// Enforce the storage.Interface guarantee that the resource version of the returned data
	// "will be at least 'resourceVersion'".
	if minimumRV > actualRevision {
		return storage.NewTooLargeResourceVersionError(minimumRV, actualRevision, 0)
	}
	return nil
}

// decode decodes value of bytes into object. It will also set the object resource version to rev.
// On success, objPtr would be set to the object.
func decode(codec runtime.Codec, versioner storage.Versioner, value []byte, objPtr runtime.Object, rev int64) error {
	if _, err := conversion.EnforcePtr(objPtr); err != nil {
		return fmt.Errorf("unable to convert output object to pointer: %v", err)
	}
	_, _, err := codec.Decode(value, nil, objPtr)
	if err != nil {
		return err
	}
	// being unable to set the version does not prevent the object from being extracted
	if err := versioner.UpdateObject(objPtr, uint64(rev)); err != nil {
		klog.Errorf("failed to update object version: %v", err)
	}
	return nil
}

// appendListItem decodes and appends the object (if it passes filter) to v, which must be a slice.
func appendListItem(v reflect.Value, data []byte, rev uint64, pred storage.SelectionPredicate, codec runtime.Codec, versioner storage.Versioner, newItemFunc func() runtime.Object) error {
	obj, _, err := codec.Decode(data, nil, newItemFunc())
	if err != nil {
		return err
	}
	// being unable to set the version does not prevent the object from being extracted
	if err := versioner.UpdateObject(obj, rev); err != nil {
		klog.Errorf("failed to update object version: %v", err)
	}
	if matched, err := pred.Matches(obj); err == nil && matched {
		v.Set(reflect.Append(v, reflect.ValueOf(obj).Elem()))
	}
	return nil
}

func notFound(key string) clientv3.Cmp {
	return clientv3.Compare(clientv3.ModRevision(key), "=", 0)
}

// getTypeName returns type name of an object for reporting purposes.
func getTypeName(obj interface{}) string {
	return reflect.TypeOf(obj).String()
}
