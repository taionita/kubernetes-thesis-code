/*
Copyright 2014 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package httplog

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	etcd3storage "k8s.io/apiserver/pkg/storage/etcd3"
	"k8s.io/client-go/tools/cache"
	"k8s.io/kubernetes/pkg/controller"
	"k8s.io/kubernetes/pkg/controller/deployment"
	"k8s.io/kubernetes/pkg/controller/replicaset"
	"k8s.io/kubernetes/pkg/scheduler"
	"k8s.io/kubernetes/pkg/scheduler/framework/plugins/defaultbinder"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
	"sync"
	"sync/atomic"
	"time"
	"k8s.io/klog/v2"
)

var CurrentRunningRequests int32
var DeployCreated int32
var RSCreated int32
var PodCreated int32
var PodBound int32


type WatcherToWrite struct {
	ArrivalTime           time.Time     `json:"ArrivalTime"`
	ResourceType			string 		`json:"ResourceType"`
	ResourceHex              string        `json:"ResourceHex"`
	Misc				string			`json:"Misc"`
}

var WatcherToWriteLog = make([]*WatcherToWrite, 0, 4000)
var WatcherToWriteLogMutex sync.Mutex



type HTTPLog struct {
	ArrivalTime           time.Time     `json:"ArrivalTime"`
	Method                string        `json:"Method"`
	URI                   string        `json:"URI"`
	Latency               time.Duration `json:"Latency"`
	UserAgent             string        `json:"UserAgent"`
	Resp                  int           `json:"Resp"`
	SincePostEtcd         time.Duration `json:"SincePostEtcd"`
	SinceHTTPHandler      time.Duration `json:"SinceHTTPHandler"`
	TxnSuccess            bool          `json:"TxnSuccess"`
	EtcdLatency           time.Duration `json:"EtcdLatency"`
	IsForSecondaryStorage bool          `json:"IsForSecondaryStorage"`
	ConcurrentRequests    int32         `json:"ConcurrentRequests"`
	ArrivalAtEtcdCall	time.Time    	`json:"ArrivalAtEtcdCall"`
	ArrivalAtEtcdDone   time.Time		`json:"ArrivalAtEtcdDone"`
	EtcdCallType  		string		`json:"EtcdCallType"`
}

var HTTPRequestLog = make([]*HTTPLog, 0, 4000)
var HTTPRequestLogMutex sync.Mutex

// StacktracePred returns true if a stacktrace should be logged for this status.
type StacktracePred func(httpStatus int) (logStacktrace bool)

type logger interface {
	Addf(format string, data ...interface{})
}

type respLoggerContextKeyType int

// respLoggerContextKey is used to store the respLogger pointer in the request context.
const respLoggerContextKey respLoggerContextKeyType = iota

// Add a layer on top of ResponseWriter, so we can track latency and error
// message sources.
//
// TODO now that we're using go-restful, we shouldn't need to be wrapping
// the http.ResponseWriter. We can recover panics from go-restful, and
// the logging value is questionable.
type respLogger struct {
	hijacked       bool
	statusRecorded bool
	status         int
	statusStack    string
	addedInfo      string
	startTime      time.Time

	captureErrorOutput bool

	req *http.Request
	w   http.ResponseWriter

	logStacktracePred StacktracePred
}

// Simple logger that logs immediately when Addf is called
type passthroughLogger struct{}

// Addf logs info immediately.
func (passthroughLogger) Addf(format string, data ...interface{}) {
	klog.V(2).Info(fmt.Sprintf(format, data...))
}

// DefaultStacktracePred is the default implementation of StacktracePred.
func DefaultStacktracePred(status int) bool {
	return (status < http.StatusOK || status >= http.StatusInternalServerError) && status != http.StatusSwitchingProtocols
}

//
//func goid() int {
//	var buf [64]byte
//	n := runtime.Stack(buf[:], false)
//	idField := strings.Fields(strings.TrimPrefix(string(buf[:n]), "goroutine "))[0]
//	id, err := strconv.Atoi(idField)
//	if err != nil {
//		panic(fmt.Sprintf("cannot get goroutine id: %v", err))
//	}
//	return id
//}
//
//func canSendToSecondaryStorage2(userAgent string, verb string, uri string) bool {
//	//we mostly want to return early for these particular URIs because they muck up later checks
//	uriSubstringForSecondary := []string{"TRIGGER=STARTOFEXPERIMENT", "TRIGGER-CONTROLLER-EXPERIMENT",
//		"WHAT=ENABLESECONDARY", "WHAT=DISABLESECONDARY"}
//	for _, subs := range uriSubstringForSecondary {
//		if strings.Contains(uri, subs) {
//			return true
//		}
//	}
//
//	uriForPrimary := []string{"/api/v1/namespaces/default", "/api/v1/namespaces/default/services/kubernetes",
//		"/api/v1/namespaces/default/endpoints/kubernetes", "/apis/discovery.k8s.io/v1beta1/namespaces/default/endpointslices/kubernetes"}
//	for _, str := range uriForPrimary {
//		if str == uri {
//			return false
//		}
//	}
//
//	// kubernetes-internal mechanisms
//	uriSubstringForPrimary := []string{"/leases/", "/nodes/"}
//	for _, subs := range uriSubstringForPrimary {
//		if strings.Contains(uri, subs) {
//			return false
//		}
//	}
//
//	userAgentSubstringForPrimary := []string{"applyall/", "kubectl/", "kubemark/"}
//	for _, subs := range userAgentSubstringForPrimary {
//		if strings.Contains(userAgent, subs) {
//			return false
//		}
//	}
//
//	uriTail := extractUriTail(uri)
//	userAgentComponent := extractUserAgentComponent(userAgent)
//
//	userAgentComponent_SubstringForPrimary := []string{"leader-election", "cronjob-controller", "generic-garbage-collector"}
//	for _, subs := range userAgentComponent_SubstringForPrimary {
//		if strings.Contains(userAgentComponent, subs) {
//			return false
//		}
//	}
//
//	// dfw[(dfw['user_agent'].str.contains('deployment') ) & (dfw['verb'] == 'POST') & (dfw['uri_tail'] != 'events') ]
//	if strings.Contains(userAgentComponent, "deployment") && verb == "POST" && uriTail != "events" {
//		return false
//		// dfw[(dfw['user_agent'].str.contains('replicaset') ) & (dfw['verb'] == 'POST') & (dfw['uri_tail'] != 'events') ]
//	} else if strings.Contains(userAgentComponent, "replicaset") && verb == "POST" && uriTail != "events" {
//		return false
//		// dfw[(dfw['user_agent'].str.contains('scheduler')) &   (dfw['uri'].str.contains('pods')) & (dfw['resp'].str.contains('20')) & (dfw['uri'].str.endswith('/binding')) ]
//	} else if strings.Contains(userAgentComponent, "scheduler") && strings.Contains(uri, "pods") && strings.Contains(uri, "/binding") {
//		return false
//	}
//	return true
//}

//
//func extractUserAgentComponent(userAgentEntire string) string {
//	userAgent2 := strings.Split(userAgentEntire, " ")[2]
//	userAgent2Split := strings.Split(userAgent2, "/")
//
//	return userAgent2Split[len(userAgent2Split)-1]
//}
//
//func extractUriTail(uri string) string {
//	if strings.Contains(uri, "namespaces/default/") {
//		uri = strings.Split(uri, "?")[0]
//		uri = strings.TrimRight(uri, "/")
//		uriSpl := strings.Split(uri, "/")
//		uri = uriSpl[len(uriSpl)-1]
//		return uri
//	} else {
//		return uri
//	}
//}

//func startShotcutChannel(uri string){
//	if strings.Contains(uri, "TRIGGER=STARTOFEXPERIMENT") {
//		etcd3.ShortcutDeploymentNotifications
//	}
//}



func checkBufferIOWriteErr(_ int, e error) {
	if e != nil {
		panic(fmt.Sprintf("bufferio error: %v", e))
	}
}

func dumpLogToFile(uri string) {


	if strings.Contains(uri, "ACTIONTODO=APISERVERDUMPTHELOGTOFILE") {
		apiserverFile := "/tmp/apiserver_logs.out"
		f, err := os.Create(apiserverFile)
		if err != nil {
			panic(fmt.Sprintf("couldn't create file %s: %v",apiserverFile, err))
		}
		defer f.Close()
		w := bufio.NewWriter(f)
		defer w.Flush()


		checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("JSON-BEGIN-MARKER\n")))
		WatcherToWriteLogMutex.Lock()
		HTTPRequestLogMutex.Lock()
		etcd3storage.WatcherIncomingLogMutex.Lock()

		for _, log := range HTTPRequestLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("LogEntryJSON: %s\n", string(thing))))
		}

		for _, log := range etcd3storage.WatcherIncomingLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("WatchEntryJSON: %s\n", string(thing))))
		}

		for _, log := range WatcherToWriteLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("WatcherToWrite: %s\n", string(thing))))
		}

		WatcherToWriteLog = nil
		HTTPRequestLog = nil
		etcd3storage.WatcherIncomingLog = nil
		WatcherToWriteLogMutex.Unlock()
		HTTPRequestLogMutex.Unlock()
		etcd3storage.WatcherIncomingLogMutex.Unlock()

		checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("JSON-END-MARKER\n")))
		return
	}

	if strings.Contains(uri, "ACTIONTODO=CONTROLLERDUMPTHELOGTOFILE") {
		controllerFile := "/tmp/controller_logs.out"
		f, err := os.Create(controllerFile)
		if err != nil {
			panic(fmt.Sprintf("couldn't create file %s: %v",controllerFile, err))
		}
		defer f.Close()
		w := bufio.NewWriter(f)
		defer w.Flush()


		checkBufferIOWriteErr(w.WriteString("JSON-BEGIN-MARKER\n"))
		deployment.ArrivalControllerLogMutex.Lock()
		for _, log := range deployment.ArrivalControllerLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDelivered: %s\n", string(thing))))
		}
		deployment.ArrivalControllerLog = nil
		deployment.ArrivalControllerLogMutex.Unlock()


		cache.ArrivalReflectorStep0Mutex.Lock()
		cache.ArrivalReflectorStep0Use = false
		for _, log := range cache.ArrivalReflectorStep0 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep0: %s\n", string(thing))))
		}

		cache.ArrivalReflectorStep0Mutex.Unlock()

		cache.ArrivalControllerReflectorStep1Mutex.Lock()
		cache.ArrivalControllerReflectorStep1Use = false
		for _, log := range cache.ArrivalControllerReflectorStep1 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep1: %s\n", string(thing))))
		}
		cache.ArrivalControllerReflectorStep1Mutex.Unlock()

		cache.ArrivalControllerReflectorStep3Mutex.Lock()
		cache.ArrivalControllerReflectorStep3Use = false
		for _, log := range cache.ArrivalControllerReflectorStep3 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep3: %s\n", string(thing))))
		}

		cache.ArrivalControllerReflectorStep3Mutex.Unlock()



		cache.ArrivalControllerReflectorStep4Mutex.Lock()
		cache.ArrivalControllerReflectorStep4Use = false
		for _, log := range cache.ArrivalControllerReflectorStep4 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep4: %s\n", string(thing))))
		}
		cache.ArrivalControllerReflectorStep4Mutex.Unlock()

		cache.ArrivalControllerReflectorStep5Mutex.Lock()
		cache.ArrivalControllerReflectorStep5Use = false
		for _, log := range cache.ArrivalControllerReflectorStep5 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep5: %s\n", string(thing))))
		}
		cache.ArrivalControllerReflectorStep5Mutex.Unlock()


		cache.ArrivalControllerReflectorStep6Mutex.Lock()
		cache.ArrivalControllerReflectorStep6Use = false
		for _, log := range cache.ArrivalControllerReflectorStep6 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep6: %s\n", string(thing))))
		}
		cache.ArrivalControllerReflectorStep6Mutex.Unlock()

		deployment.ArrivalDeploymentCreateRSMutex.Lock()
		for _, log := range deployment.ArrivalDeploymentCreateRSLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("ArrivalDeploymentCreateRS: %s\n", string(thing))))
		}
		deployment.ArrivalDeploymentCreateRSLog = nil
		deployment.ArrivalDeploymentCreateRSMutex.Unlock()

		replicaset.ArrivalRSControllerMutex.Lock()
		for _, log := range replicaset.ArrivalRSControllerLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("ArrivalRSController: %s\n", string(thing))))
		}
		replicaset.ArrivalRSControllerLog = nil
		replicaset.ArrivalRSControllerMutex.Unlock()

		controller.ArrivalRSCreatePodMutex.Lock()
		for _, log := range controller.ArrivalRSCreatePodLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("ArrivalRSCreatePod: %s\n", string(thing))))
		}
		controller.ArrivalRSCreatePodLog = nil
		controller.ArrivalRSCreatePodMutex.Unlock()
		checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("JSON-END-MARKER\n")))
		return
	}

	if strings.Contains(uri, "ACTIONTODO=SCHEDULERDUMPTHELOGTOFILE") {
		schedulerFile := "/tmp/scheduler_logs.out"
		f, err := os.Create(schedulerFile)
		if err != nil {
			panic(fmt.Sprintf("couldn't create file %s: %v",schedulerFile, err))
		}
		defer f.Close()
		w := bufio.NewWriter(f)
		defer w.Flush()

		checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("JSON-BEGIN-MARKER\n")))

		cache.ArrivalReflectorStep0Mutex.Lock()
		cache.ArrivalReflectorStep0Use = false
		for _, log := range cache.ArrivalReflectorStep0 {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDeliveryStep0: %s\n", string(thing))))
		}

		cache.ArrivalReflectorStep0Mutex.Unlock()


		scheduler.ArrivalSchedulerLogMutex.Lock()


		for _, log := range scheduler.ArrivalSchedulerLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("NotifDelivered: %s\n", string(thing))))
		}

		scheduler.ArrivalSchedulerLog = nil
		scheduler.ArrivalSchedulerLogMutex.Unlock()



		defaultbinder.ArrivalSchedulerBindPodMutex.Lock()
		for _, log := range defaultbinder.ArrivalSchedulerBindPodLog {

			thing, err := json.Marshal(log)
			if err != nil {
				panic(fmt.Sprintf("json marshall err in dumpLogToFile: %v", err))
			}
			checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("ArrivalSchedulerBindPodLog: %s\n", string(thing))))
		}
		defaultbinder.ArrivalSchedulerBindPodLog = nil
		defaultbinder.ArrivalSchedulerBindPodMutex.Unlock()

		checkBufferIOWriteErr(w.WriteString(fmt.Sprintf("JSON-END-MARKER\n")))
	}

}

func checkStartLogging(uri string) {
	if strings.Contains(uri, "TRIGGER=APISERVERSTARTOFEXPERIMENT") {
		PodBound = 0
		DeployCreated = 0
		RSCreated = 0
		PodCreated = 0
		HTTPRequestLogMutex.Lock()
		etcd3storage.WatcherIncomingLogMutex.Lock()
		WatcherToWriteLogMutex.Lock()

		HTTPRequestLog = make([]*HTTPLog, 0, 4000)
		etcd3storage.WatcherIncomingLog = make([]*etcd3storage.WatcherLog, 0, 2000)
		WatcherToWriteLog = make([]*WatcherToWrite, 0, 4000)

		WatcherToWriteLogMutex.Unlock()
		HTTPRequestLogMutex.Unlock()
		etcd3storage.WatcherIncomingLogMutex.Unlock()


		return
	}

	if strings.Contains(uri, "TRIGGER=CONTROLLERSTARTOFEXPERIMENT") {
		deployment.ArrivalControllerLogMutex.Lock()
		deployment.ArrivalControllerLog = make([]*deployment.ArrivalControllerLogEntry, 0, 2000)
		deployment.ArrivalControllerLogMutex.Unlock()

		cache.ArrivalReflectorStep0Mutex.Lock()
		cache.ArrivalReflectorStep0 = make([]*cache.ArrivalReflectorStep0Entry, 0, 3000)
		cache.ArrivalReflectorStep0Use = true
		cache.ArrivalReflectorStep0Mutex.Unlock()

		cache.ArrivalControllerReflectorStep1Mutex.Lock()
		cache.ArrivalControllerReflectorStep1 = make([]*cache.ArrivalControllerReflectorStep1Entry, 0, 3000)
		cache.ArrivalControllerReflectorStep1Use = true
		cache.ArrivalControllerReflectorStep1Mutex.Unlock()

		cache.ArrivalControllerReflectorStep3Mutex.Lock()
		cache.ArrivalControllerReflectorStep3 = make([]*cache.ArrivalControllerReflectorStep3Entry, 0, 3000)
		cache.ArrivalControllerReflectorStep3Use = true
		cache.ArrivalControllerReflectorStep3Mutex.Unlock()

		cache.ArrivalControllerReflectorStep4Mutex.Lock()
		cache.ArrivalControllerReflectorStep4 = make([]*cache.ArrivalControllerReflectorStep4Entry, 0, 3000)
		cache.ArrivalControllerReflectorStep4Use = true
		cache.ArrivalControllerReflectorStep4Mutex.Unlock()

		cache.ArrivalControllerReflectorStep5Mutex.Lock()
		cache.ArrivalControllerReflectorStep5 = make([]*cache.ArrivalControllerReflectorStep5Entry, 0, 3000)
		cache.ArrivalControllerReflectorStep5Use = true
		cache.ArrivalControllerReflectorStep5Mutex.Unlock()

		cache.ArrivalControllerReflectorStep6Mutex.Lock()
		cache.ArrivalControllerReflectorStep6 = make([]*cache.ArrivalControllerReflectorStep6Entry, 0, 3000)
		cache.ArrivalControllerReflectorStep6Use = true
		cache.ArrivalControllerReflectorStep6Mutex.Unlock()

		deployment.ArrivalDeploymentCreateRSMutex.Lock()
		deployment.ArrivalDeploymentCreateRSLog = make([]*deployment.ArrivalDeploymentCreateRSEntry, 0, 2000)
		deployment.ArrivalDeploymentCreateRSMutex.Unlock()

		replicaset.ArrivalRSControllerMutex.Lock()
		replicaset.ArrivalRSControllerLog = make([]*replicaset.ArrivalRSControllerEntry, 0, 2000)
		replicaset.ArrivalRSControllerMutex.Unlock()

		controller.ArrivalRSCreatePodMutex.Lock()
		controller.ArrivalRSCreatePodLog = make([]*controller.ArrivalRSCreatePodEntry, 0, 2000)//
		controller.ArrivalRSCreatePodMutex.Unlock()

		return
	}

	if strings.Contains(uri, "TRIGGER=SCHEDULERSTARTOFEXPERIMENT") {

		cache.ArrivalReflectorStep0Mutex.Lock()
		cache.ArrivalReflectorStep0 = make([]*cache.ArrivalReflectorStep0Entry, 0, 3000)
		cache.ArrivalReflectorStep0Use = true
		cache.ArrivalReflectorStep0Mutex.Unlock()

		scheduler.ArrivalSchedulerLogMutex.Lock()
		scheduler.ArrivalSchedulerLog = make([]*scheduler.ArrivalSchedulerLogEntry, 0, 2000)
		scheduler.ArrivalSchedulerLogMutex.Unlock()

		defaultbinder.ArrivalSchedulerBindPodMutex.Lock()
		defaultbinder.ArrivalSchedulerBindPodLog = make([]*defaultbinder.ArrivalSchedulerBindPodEntry, 0, 2000)
		defaultbinder.ArrivalSchedulerBindPodMutex.Unlock()

		return
	}
}

func canSendToSecondaryStorage(uri string) bool {
	uriSubstringForSecondary := []string{"TRIGGER=APISERVERSTARTOFEXPERIMENT", "TRIGGER-CONTROLLER-EXPERIMENT",
		"WHAT=ENABLESECONDARY", "WHAT=DISABLESECONDARY"}
	for _, subs := range uriSubstringForSecondary {
		if strings.Contains(uri, subs) {
			return true
		}
	}

	if strings.Contains(uri, "StorageHint=SECONDARY") {
		return true
	}

	if strings.HasSuffix(uri, "/default/events") {
		return true
	}

	return false
}

func checkIfTriggerSecondaryStorage(uri string) {

	if strings.Contains(uri, "WHAT=ENABLESECONDARY") {
		klog.InfoS("ENABLESECONDARY HTTP call: will store in secondary etcd")
		etcd3storage.UseSecondaryStorage = true
		return
	}
	if strings.Contains(uri, "WHAT=DISABLESECONDARY") {
		klog.InfoS("DISABLESECONDARY HTTP call: will store only in main and nothing in secondary etcd")
		etcd3storage.UseSecondaryStorage = false
		return
	}

}

func countEvent(verb, uri, userAgent string) {
	if strings.HasSuffix(uri, "/default/events") {
		return
	}

	if verb == "POST" {

		// external client creating the initial deploy objects
		if (strings.HasPrefix(userAgent, "applyall") || strings.HasPrefix(userAgent, "applystream") || strings.HasPrefix(userAgent, "kuebctl") ) &&
			strings.HasPrefix(uri, "/apis/apps/v1/namespaces/default/deployments") {

			atomic.AddInt32(&DeployCreated, 1)
			return
		}

		// Deployment controller creating replicaset objects
		if strings.HasSuffix(userAgent, "/deployment-controller") && strings.HasPrefix(uri, "/apis/apps/v1/namespaces/default/replicasets") {

			atomic.AddInt32(&RSCreated, 1)
			return
		}

		// Replicaset controller creating pod objects
		if strings.HasSuffix(userAgent, "/replicaset-controller") && strings.HasPrefix(uri, "/api/v1/namespaces/default/pods") {

			atomic.AddInt32(&PodCreated, 1)
			return
		}

		// Scheduler binding pod objects to nodes
		if strings.HasSuffix(userAgent, "/scheduler") && strings.HasPrefix(uri, "/api/v1/namespaces/default/pods/") && strings.HasSuffix(uri, "/binding") {

			atomic.AddInt32(&PodBound, 1)
			return
		}


	}

}

// WithLogging wraps the handler with logging.
func WithLogging(handler http.Handler, pred StacktracePred) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		//reqBody, _ := ioutil.ReadAll(req.Body)
		//req.Body = ioutil.NopCloser(bytes.NewBuffer(reqBody))

		ctx := req.Context()
		checkIfTriggerSecondaryStorage(req.RequestURI)
		dumpLogToFile(req.RequestURI)
		checkStartLogging(req.RequestURI)
		if strings.Contains(req.RequestURI, "APISERVERGIMMETHEBINDCOUNT") {
			bindCount := []byte(fmt.Sprintf("DEPLOY:%d\nRS:%d\nPOD:%d\nBIND:%d\n", DeployCreated, RSCreated, PodCreated, PodBound))
			w.WriteHeader(200)
			_ , err := w.Write(bindCount)
			if err != nil {
				panic(fmt.Sprintf("cannot write bind count back: %v", err))
			}
			return
		}

		dict := make(map[string]interface{})
		arrivalTime := time.Now()
		dict["http-latency-time"] = arrivalTime
		isForSecondaryStorage := canSendToSecondaryStorage(req.RequestURI)
		//klog.Infof(" IsForSecondaryStorage: %t, user agent [%s], method [%s], URI [%s]", IsForSecondaryStorage, req.UserAgent(), req.Method, req.RequestURI)
		dict["is-for-secondary-storage"] = isForSecondaryStorage
		ctx = context.WithValue(ctx, "dict", dict)

		if old := respLoggerFromContext(req); old != nil {
			panic("multiple WithLogging calls!")
		}
		rl := newLogged(req, w).StacktraceWhen(pred)
		req = req.WithContext(context.WithValue(ctx, respLoggerContextKey, rl))

		atomic.AddInt32(&CurrentRunningRequests, 1)
		concurrentRequests := atomic.LoadInt32(&CurrentRunningRequests)

		defer func() {
			atomic.AddInt32(&CurrentRunningRequests, -1)

			var sincePostEtcdDuration time.Duration
			var sinceHttpLatencyFunction time.Duration
			var conditionalIfTnxSucceeded bool
			var etcdLatency time.Duration
			var etcdCallType string

			sincePostEtcdDuration = 0
			dictCtx := ctx.Value("dict")
			if dictCtx != nil {
				endOfReq := time.Now()
				dict := dictCtx.(map[string]interface{})
				var ArrivalAtEtcdCall time.Time
				var ArrivalAtEtcdDone time.Time

				sincePostEtcdInterface, exist := dict["sincePostEtcd"]
				if exist {
					sincePostEtcd := sincePostEtcdInterface.(time.Time)
					sincePostEtcdDuration = endOfReq.Sub(sincePostEtcd)

					ArrivalAtEtcdCall = dict["httpArrivalAtEtcdCall"].(time.Time)
					ArrivalAtEtcdDone = dict["httpArrivalAtEtcdDone"].(time.Time)
				}
				sinceHttpLatencyFunctionInterface, exist := dict["sinceHttpLatencyFunction"]
				if exist {
					sinceHttpLatencyFunction = sinceHttpLatencyFunctionInterface.(time.Duration)
				}
				conditionalIfTnxSucceededInterface, exist := dict["conditionalIfTnxSucceeded"]
				if exist {
					conditionalIfTnxSucceeded = conditionalIfTnxSucceededInterface.(bool)
				}
				etcdLatencyInterface, exist := dict["etcdLatency"]
				if exist {
					etcdLatency = etcdLatencyInterface.(time.Duration)
				}
				etcdCallTypeInterface, exist := dict["etcdCallType"]
				if exist {
					etcdCallType = etcdCallTypeInterface.(string)
				}


				var logEntry = &HTTPLog{
					ArrivalTime:           arrivalTime,
					Method:                rl.req.Method,
					URI:                   rl.req.RequestURI,
					Latency:               endOfReq.Sub(arrivalTime),
					UserAgent:             rl.req.UserAgent(),
					Resp:                  rl.status,
					SincePostEtcd:         sincePostEtcdDuration,
					SinceHTTPHandler:      sinceHttpLatencyFunction,
					TxnSuccess:            conditionalIfTnxSucceeded,
					EtcdLatency:           etcdLatency,
					IsForSecondaryStorage: isForSecondaryStorage,
					ConcurrentRequests:    concurrentRequests,
					ArrivalAtEtcdCall: 	   ArrivalAtEtcdCall,
					ArrivalAtEtcdDone:     ArrivalAtEtcdDone,
					EtcdCallType:  			etcdCallType,
				}

				if rl.status == 200 || rl.status == 201 {
					countEvent(rl.req.Method, rl.req.RequestURI, rl.req.UserAgent())
				}

				HTTPRequestLogMutex.Lock()
				if HTTPRequestLog != nil {
					HTTPRequestLog = append(HTTPRequestLog, logEntry)
				}
				HTTPRequestLogMutex.Unlock()
			}
			// klog.InfoS("HTTP", rl.LogArgs(sincePostEtcdDuration, sinceHttpLatencyFunction, conditionalIfTnxSucceeded, etcdLatency, isForSecondaryStorage, concurrentRequests)...)
		}()
		if klog.V(3).Enabled() {
			//defer func() { klog.InfoS("HTTP", rl.LogArgs()...) }()
			//defer func() { klog.InfoS("HTTP", rl.LogArgs(uuidTracking)...) }()
		}
		handler.ServeHTTP(rl, req)
	})
}

// respLoggerFromContext returns the respLogger or nil.
func respLoggerFromContext(req *http.Request) *respLogger {
	ctx := req.Context()
	val := ctx.Value(respLoggerContextKey)
	if rl, ok := val.(*respLogger); ok {
		return rl
	}
	return nil
}

// newLogged turns a normal response writer into a logged response writer.
func newLogged(req *http.Request, w http.ResponseWriter) *respLogger {
	return &respLogger{
		startTime:         time.Now(),
		req:               req,
		w:                 w,
		logStacktracePred: DefaultStacktracePred,
	}
}

// LogOf returns the logger hiding in w. If there is not an existing logger
// then a passthroughLogger will be created which will log to stdout immediately
// when Addf is called.
func LogOf(req *http.Request, w http.ResponseWriter) logger {
	if rl := respLoggerFromContext(req); rl != nil {
		return rl
	}
	return &passthroughLogger{}
}

// Unlogged returns the original ResponseWriter, or w if it is not our inserted logger.
func Unlogged(req *http.Request, w http.ResponseWriter) http.ResponseWriter {
	if rl := respLoggerFromContext(req); rl != nil {
		return rl.w
	}
	return w
}

// StacktraceWhen sets the stacktrace logging predicate, which decides when to log a stacktrace.
// There's a default, so you don't need to call this unless you don't like the default.
func (rl *respLogger) StacktraceWhen(pred StacktracePred) *respLogger {
	rl.logStacktracePred = pred
	return rl
}

// StatusIsNot returns a StacktracePred which will cause stacktraces to be logged
// for any status *not* in the given list.
func StatusIsNot(statuses ...int) StacktracePred {
	statusesNoTrace := map[int]bool{}
	for _, s := range statuses {
		statusesNoTrace[s] = true
	}
	return func(status int) bool {
		_, ok := statusesNoTrace[status]
		return !ok
	}
}

// Addf adds additional data to be logged with this request.
func (rl *respLogger) Addf(format string, data ...interface{}) {
	rl.addedInfo += "\n" + fmt.Sprintf(format, data...)
}

//func (rl *respLogger) LogArgs(body string) []interface{} {
func (rl *respLogger) LogArgs(sincePostEtcdDuration time.Duration, sinceHttpLatencyFunction time.Duration,
	conditionalIfTnxSucceeded bool, etcdLatency time.Duration, isForSecondaryStorage bool, concurrentRequests int32) []interface{} {
	latency := time.Since(rl.startTime)
	if rl.hijacked {
		return []interface{}{
			"verb", rl.req.Method,
			"URI", rl.req.RequestURI,
			"latency", latency,
			"userAgent", rl.req.UserAgent(),
			"srcIP", rl.req.RemoteAddr,
			"hijacked", true,
		}
	}

	args := []interface{}{
		"verb", rl.req.Method,
		"URI", rl.req.RequestURI,
		"latency", latency,
		"userAgent", rl.req.UserAgent(),
		"srcIP", rl.req.RemoteAddr,
		"resp", fmt.Sprintf("%d", rl.status),
		"sincePostEtcd", fmt.Sprintf("%v", sincePostEtcdDuration),
		"sinceHttpLatencyFunction", fmt.Sprintf("%v", sinceHttpLatencyFunction),
		"tnxCompareSucceed", fmt.Sprintf("%t", conditionalIfTnxSucceeded),
		"etcdLatency", fmt.Sprintf("%v", etcdLatency),
		"isForSecondaryStorage", fmt.Sprintf("%t", isForSecondaryStorage),
		"concurrentRequests", fmt.Sprintf("%d", concurrentRequests),
	}
	if len(rl.statusStack) > 0 {
		args = append(args, "statusStack", rl.statusStack)
	}

	if len(rl.addedInfo) > 0 {
		args = append(args, "addedInfo", rl.addedInfo)
	}
	return args
}

// Header implements http.ResponseWriter.
func (rl *respLogger) Header() http.Header {
	return rl.w.Header()
}

// Write implements http.ResponseWriter.
func (rl *respLogger) Write(b []byte) (int, error) {
	if !rl.statusRecorded {
		rl.recordStatus(http.StatusOK) // Default if WriteHeader hasn't been called
	}
	if rl.captureErrorOutput {
		rl.Addf("logging error output: %q\n", string(b))
	}
	return rl.w.Write(b)
}

// Flush implements http.Flusher even if the underlying http.Writer doesn't implement it.
// Flush is used for streaming purposes and allows to flush buffered data to the client.
func (rl *respLogger) Flush() {
	if flusher, ok := rl.w.(http.Flusher); ok {
		flusher.Flush()
	} else if klog.V(2).Enabled() {
		klog.InfoDepth(1, fmt.Sprintf("Unable to convert %+v into http.Flusher", rl.w))
	}
}

// WriteHeader implements http.ResponseWriter.
func (rl *respLogger) WriteHeader(status int) {
	rl.recordStatus(status)
	rl.w.WriteHeader(status)
}

// Hijack implements http.Hijacker.
func (rl *respLogger) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	rl.hijacked = true
	return rl.w.(http.Hijacker).Hijack()
}

// CloseNotify implements http.CloseNotifier
func (rl *respLogger) CloseNotify() <-chan bool {
	return rl.w.(http.CloseNotifier).CloseNotify()
}

func (rl *respLogger) recordStatus(status int) {
	rl.status = status
	rl.statusRecorded = true
	if rl.logStacktracePred(status) {
		// Only log stacks for errors
		stack := make([]byte, 50*1024)
		stack = stack[:runtime.Stack(stack, false)]
		rl.statusStack = "\n" + string(stack)
		rl.captureErrorOutput = true
	} else {
		rl.statusStack = ""
	}
}
